import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-stage',
  templateUrl: './stage.component.html',
  styleUrls: ['./stage.component.scss']
})
export class StageComponent implements OnInit {
  emailbodyview= "html" //html/preview
  previewcontent = ""
  htmlcontent=""
  @Input() stage:any;
  @Input() activestage:any;
  @Output() onSaveStage = new EventEmitter<any>();
  @Output() onDeleteStage = new EventEmitter<any>();
  constructor() { }

  ngOnInit() {
    console.log("ngOnInit stage.component.ts")
    this.htmlcontent=this.stage.body
  }
  onselectbody(item){
    console.log("in onselectbody()")
    this.emailbodyview=item;
    if(item === 'html'){

    }
    else{
      this.previewcontent=this.htmlcontent
    }
  }
  onSubmit(form,stageNumber,event){
    console.log("stageNumber ",stageNumber)
    console.log("in onSubmit ",form.value)
    if(form.value.subject.length >5 || form.value.body.length >5){
      if(form.value.scheduleType == "immediately"){
        form.value.afterCount = 0
        form.value.after = ""
      }
      let val = {...form.value,stage:stageNumber,}
      console.log("val ",val)
      this.onSaveStage.emit(val);
    }
    else{
      alert("please enter valid details in subject/body")
    }
  }
  onDelete(){
    console.log("onDelete ",this.stage.stage)
    this.onDeleteStage.emit({stage:this.stage.stage})
  }
}
