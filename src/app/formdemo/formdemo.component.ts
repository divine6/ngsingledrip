import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-formdemo',
  templateUrl: './formdemo.component.html',
  styleUrls: ['./formdemo.component.scss']
})
export class FormdemoComponent implements OnInit {
  @Input() teamindex:any;
  @Output() teamdata = new EventEmitter<{teamindex:number,teamname : string, country : string}>();
  constructor() { }

  ngOnInit() {
  }
  onSubmit(form){
    /*
    let firstname = form.firstname.value
    let lastname = form.lastname.value
    let city = form.city.value
    console.log("firstname ",firstname)
    console.log("lastname ",lastname)
    console.log("city ",city)
    */
   console.log("onSubmit ",form.value)
   //this.teamdata.emit({teamindex: this.teamindex,teamname : form.value.teamname,country : form.value.country})
   this.teamdata.emit({teamindex: this.teamindex, ...form.value})
  }
}
