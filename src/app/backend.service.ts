import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  url = "http://localhost:4900/api/adminpanel/singledrip";
  
  constructor(private http: HttpClient) { }


  getAllSingleDrips(){
    return this.http.get(this.url);
  }
  updateDripStatus(dripid,status){
    return this.http.put(this.url+'/'+dripid+'/'+status,{});
  }
  deleteDrip(dripid){
    return this.http.delete(this.url+'/'+dripid);
  }
  getDripById(dripid){
    return this.http.get(this.url+'/'+dripid);
  }
  updateDrip(dripid,body){
    return this.http.put(this.url+'/'+dripid,body); 
  }
  saveDrip(body){
    return this.http.post(this.url,body); 
  }
}
