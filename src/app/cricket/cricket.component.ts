import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cricket',
  templateUrl: './cricket.component.html',
  styleUrls: ['./cricket.component.scss']
})
export class CricketComponent implements OnInit {
  teamCount = 3
  constructor() { }
  teamdata=[]
  ngOnInit() {
  }
  onteamdata(event){
    console.log("onteamdata ",event)
    if(this.teamdata[event.teamindex]){
      //this.teamdata[event.teamindex] = {teamname : event.teamname,country : event.country}
      this.teamdata[event.teamindex] =event
    }
    else{
      this.teamdata.push(event)
      //this.teamdata.push({teamname : event.teamname,country : event.country})
    }
  }
}
