import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormdemoComponent} from './formdemo/formdemo.component'
import {BuilddripComponent} from "./builddrip/builddrip.component"
import {CricketComponent} from './cricket/cricket.component'
import {ViewdripComponent} from './viewdrip/viewdrip.component'
const routes: Routes = [
  {path:'singledrip',component:ViewdripComponent},
  {path:'singledrip/cru',component:BuilddripComponent},
  {path:'singledrip/cru/:dripid',component:BuilddripComponent},

  {path:'form',component:FormdemoComponent},
  {path:'cricket',component:CricketComponent},
  //{path : '**',redirectTo: 'singledrip/crud'}
  {path : '**',redirectTo: 'singledrip'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
