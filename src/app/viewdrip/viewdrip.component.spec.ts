import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdripComponent } from './viewdrip.component';

describe('ViewdripComponent', () => {
  let component: ViewdripComponent;
  let fixture: ComponentFixture<ViewdripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewdripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
