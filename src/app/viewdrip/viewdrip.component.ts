import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

import {BackendService} from './../backend.service'
@Component({
  selector: 'app-viewdrip',
  templateUrl: './viewdrip.component.html',
  styleUrls: ['./viewdrip.component.scss']
})
export class ViewdripComponent implements OnInit {
  singleDripCollection=[]
  constructor(private backendService : BackendService,private router:Router,
    private activatedRoute:ActivatedRoute) { }

  ngOnInit() {
    this.onPageLoad()
    
  }
  onPageLoad(){
    let self=this
    this.backendService.getAllSingleDrips().subscribe(
      (data:any)=>{
        console.log("getAllSingleDrips data ",data)
        self.singleDripCollection=data;
      },
      (error:any)=>{
        console.log("error ",error)
      }
    )
  }
  onIsActiveChanged(event){
    console.log("in onIsActiveChanged ",event.target)
    let {value,id} = event.target;
    let validid = id.substring(0,id.length-1)
    console.log("value ",value," id ",id, " id ",validid)
    this.backendService.updateDripStatus(validid,value).subscribe(
      (data:any)=>{
        console.log("onIsActiveChanged data ",data)
      }
    )
  }
  deleteDrip(dripId){
    console.log("in deleteDrip ",dripId)
    this.backendService.deleteDrip(dripId).subscribe(
      (data:any)=>{
        console.log("deleteDrip data ",data)
        this.onPageLoad()
      }
    )
  }
  viewUpdateDrip(dripId){
    console.log("in viewUpdateDrip ",dripId)
    this.router.navigateByUrl('singledrip/cru/'+dripId);
  }
  //this.router.navigateByUrl('/login/register');

}
