import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BuilddripComponent } from './builddrip/builddrip.component';

import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { StageComponent } from './stage/stage.component';
import { FormdemoComponent } from './formdemo/formdemo.component';
import { CricketComponent } from './cricket/cricket.component';
import { ViewdripComponent } from './viewdrip/viewdrip.component'

@NgModule({
  declarations: [
    AppComponent,
    BuilddripComponent,
    StageComponent,
    FormdemoComponent,
    CricketComponent,
    ViewdripComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
