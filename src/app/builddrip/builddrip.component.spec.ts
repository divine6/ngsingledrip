import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuilddripComponent } from './builddrip.component';

describe('BuilddripComponent', () => {
  let component: BuilddripComponent;
  let fixture: ComponentFixture<BuilddripComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuilddripComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuilddripComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
