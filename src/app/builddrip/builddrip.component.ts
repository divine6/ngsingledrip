import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {BackendService} from './../backend.service'

@Component({
  selector: 'app-builddrip',
  templateUrl: './builddrip.component.html',
  styleUrls: ['./builddrip.component.scss']
})
export class BuilddripComponent implements OnInit {
  dripCampaignName=""
  dripid = null
  dripObject={
    dripCampaignName : "",
    stages:[

    ],
  }
  /*
  stage={
    "date": "", //exactdate when the email should be triggered
    "subject": "",
    "body": "", //emailBody
    "stage": 1, // default 1
    "datetime": "", //1 days (afterCount + after)
    "scheduleType": "afterTime", //afterTime/immediately
    "mode": "email",
    "afterCount": 0, //1
    "after": "", // days
  }
  */
 /*
  stage={
    "subject": "",
    "body": "", //emailBody
    "stage": 1, // default 1
    "datetime": "", //1 days (afterCount + after)
    "scheduleType": "afterTime", //afterTime/immediately
    "mode": "email",
    "afterCount": 0, //1
    "after": "", // days
  }
  */
 stage={
    "subject": "",
    "body": "", //emailBody
    "stage": 1, // default 1
    "scheduleType": "afterTime", //afterTime/immediately
    "mode": "email",
    "afterCount": 0, //1
    "after": "days", // days
  }
  activestage = "stage1"
  constructor(private backendService : BackendService,private activatedRoute:ActivatedRoute,
    private router:Router) { }

  ngOnInit() {
    console.log("builddrip.component.ts ngOnInit ")
    this.addStage()
    let self=this
    window.addEventListener("scroll", function(event) {
      console.log("scrolled")
      self.changeScrollHighlight()
    }, false);
    self.activatedRoute.params.subscribe(
      (params)=>{
        console.log("home params ",params);
        if(params.dripid){
          let dripid = params.dripid
          self.backendService.getDripById(dripid).subscribe(
            (data:any)=>{
              console.log("getDripById data ",data)
              self.dripCampaignName=data.dripCampaignName;
              self.dripid = data._id
              self.dripObject=data;
            },
            (error:any)=>{
              console.log("error ",error)
            }
          )
        }
      }
    )
  }
  changeScrollHighlight(){
    let stageElementPosition = []
    this.dripObject.stages.forEach((stage,index)=>{
      let t = document.getElementById("stage"+stage.stage)
      if(t){
        stageElementPosition.push(t.offsetTop)
      }
      
    })
    let windowLocation=window.pageYOffset
    for(let i=0;i<stageElementPosition.length;i++){
      if(windowLocation+180> stageElementPosition[i]){
        this.activestage="stage"+(i+1)
      }
    }
    
  }
  addStage(){
    console.log("in addStage")
    let stage = Object.assign({},this.stage)
    let stagesLength = this.dripObject.stages.length
    stage.stage=stagesLength+1
    this.dripObject.stages.push(stage)
  }
  scrollToStage(stageNumber){
    let offsetTop = document.getElementById("stage"+stageNumber).offsetTop;
    this.activestage= "stage"+stageNumber
    console.log("this.activestage ",this.activestage)
    window.scrollTo({top: offsetTop, behavior: "smooth"});
  }
  onSaveStage(event){
    console.log("onSaveStage ",event)
    
    let foundIndex = this.dripObject.stages.findIndex((stage,index)=>{
      return event.stage == stage.stage
    })
    console.log("before this.dripObject.stages ",JSON.stringify(this.dripObject))
    console.log("foundIndex ",foundIndex)

    if(foundIndex>=0){
      this.dripObject.stages[foundIndex] = {...event}
      console.log("this.dripObject.stages[foundIndex] ",this.dripObject.stages[foundIndex])
    }
    else{
      this.dripObject.stages.push = event
    }
    console.log("after this.dripObject.stages ",JSON.stringify(this.dripObject))

  }
  onDeleteStage(event){
    console.log("onDeleteStage-event ",event)
    if(this.dripObject.stages.length>1 && event.stage !==1){
      console.log("there is more than 1 stage")
      console.log("before this.dripObject.stages ",this.dripObject.stages)
      let indexOfTheStageToBeDeleted = this.dripObject.stages.findIndex((stage,index)=>{
        return stage.stage === event.stage
      })
      this.dripObject.stages = this.dripObject.stages.filter((stage,index)=>{
        return stage.stage !=event.stage
      })
      for(let i=indexOfTheStageToBeDeleted;i<this.dripObject.stages.length;i++){
        this.dripObject.stages[i].stage=i+1;
      }
      console.log("after this.dripObject.stages ",this.dripObject.stages)
    }
  }
  savedrip(){
    console.log("in savedrip()")
    this.dripObject.dripCampaignName=this.dripCampaignName
    let self=this
    console.log("this.dripObject ",JSON.stringify(this.dripObject))
    if(this.dripObject.dripCampaignName.length>=3){
      let promptResponse = prompt("Have you saved all your stages?","Enter 1 to continue, cancel to recheck");
      if(promptResponse == "1"){
        if(self.dripid){
          this.backendService.updateDrip(this.dripid,this.dripObject).subscribe(
            (data:any)=>{
              console.log("updateDrip data ",data)
              self.router.navigateByUrl('/singledrip');
            },
            (error:any)=>{
              console.log("error ",error)
            }
          )
        }
        else{
          this.backendService.saveDrip(this.dripObject).subscribe(
            (data:any)=>{
              console.log("savedrip data ",data)
              self.router.navigateByUrl('/singledrip');
            },
            (error:any)=>{
              console.log("error ",error)
            }
          )
        }
      }
      else{
        alert("kindly click save button which is present on all the stages to persist your changes")
      }
    }
    else{
      alert("please enter a valid campaign name")
    }
  }
}
